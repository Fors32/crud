<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';


    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'introduction', 'description', 'slug', 'characteristics', 'price', 'real_price', 'old_price', 'old_link', 'sizes', 'published_at', 'published', 'category_id'];

	public function category()
	{
		return $this->hasOne('App\Category', 'id', 'category_id');
	}
    
}
