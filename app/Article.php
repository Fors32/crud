<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Article extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'articles';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'introduction', 'description', 'slug', 'meta_title', 'meta_descr', 'meta_keywords', 'meta_robots', 'published', 'published_at'];

    public function posts()
    {
        return $this->hasMany('Posts');
    }
    
}
