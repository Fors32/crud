<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name')->nullable();
            $table->string('introduction')->nullable();
            $table->longText('description')->nullable();
            $table->string('slug')->nullable();
            $table->json('characteristics')->nullable();
            $table->string('price')->nullable();
            $table->string('real_price')->nullable();
            $table->string('old_price')->nullable();
            $table->text('old_link')->nullable();
            $table->json('sizes')->nullable();
            $table->dateTime('published_at')->nullable();
            $table->integer('published')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
