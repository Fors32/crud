<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'roles'], 'roles' => 'admin'], function () {
	Route::get('/', 'Admin\AdminController@index');
	Route::resource('/roles', 'Admin\RolesController');
	Route::resource('/permissions', 'Admin\PermissionsController');
	Route::resource('/users', 'Admin\UsersController');
	Route::get('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
	Route::post('/generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);
//	Auth::routes();

	Route::get('/home', 'HomeController@index')->name('home');
});


Route::resource('admin/articles', 'Admin\\ArticlesController');
Route::resource('admin/products', 'Product\\ProductsController');
Route::resource('admin/category', 'Admin\\CategoryController');